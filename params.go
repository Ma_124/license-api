package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/schema"
	"gitlab.com/Ma_124/license-api/theme"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/samclarke/robotstxt"
	"gitlab.com/Ma_124/license-api/data"
)

var RobotsTxt = true
var RobotsTxtErrAllow = true
var SchemaDecoder = schema.NewDecoder()

func ma124sParams() *data.CopyrightInfo {
	return &data.CopyrightInfo{
		Name:   "Ma_124",
		Mail:   "ma_124@outlook.com",
		Site:   "ma124.js.org",
		Gender: "male",
	}
}

func init() {
	SchemaDecoder.IgnoreUnknownKeys(true)
	SchemaDecoder.SetAliasTag("query")
}

func GetParams(pathstr string) (*data.CopyrightInfo, error) {
	// Path normalization
	if pathstr[0] == '/' {
		pathstr = pathstr[1:]
	}

	if pathstr == "" {
		return nil, fmt.Errorf("calling from unknown URL (root)")
	}

	if pathstr[len(pathstr)-1] == '/' {
		pathstr = pathstr[:len(pathstr)-1]
	}

	path := strings.Split(pathstr, "/")

	params := &data.CopyrightInfo{}
	basePathIndex := 0

	if path[0] == "l" {
		basePathIndex = 0
	} else if path[0] == ".netlify" {
		basePathIndex = 2
	} else {
		return nil, fmt.Errorf("calling from unknown URL")
	}

	// License name normalization
	if len(path) > basePathIndex+1 {
		params.License = strings.ToLower(path[basePathIndex+1])

		if !strings.Contains(params.License, ".") {
			params.License += ".html"
		}
	} else {
		return nil, fmt.Errorf("no license name specified")
	}

	if len(path) > basePathIndex+2 {
		nes := strings.Split(path[basePathIndex+2], "~")
		params.Name = nes[0]

		if len(nes) > 1 {
			params.Site = nes[1]
		}

		if len(nes) > 2 {
			params.Mail = nes[2]
		}

		if params.Name == "" {
			if params.Site != "" {
				paramz := fetchParams(params.Site)
				if paramz == nil {
					params.Name = params.Site
				} else {
					paramz.Year = params.Year
					paramz.License = params.License
					params = paramz
				}
			} else if params.Mail != "" {
				params.Name = params.Mail
			} else {
				params = getOwnersParams(params)
			}
		}
	} else {
		params = getOwnersParams(params)
	}

	// Year Logic
	if len(path) > basePathIndex+3 {
		params.Year = path[basePathIndex+3]
		if params.Year[0] == '@' {
			params.Year = params.Year[1:]
		} else if !strings.Contains(params.Year, "-") {
			y := strconv.Itoa(time.Now().Year())
			if params.Year != y {
				params.Year += "-" + y
			}
		}
	} else {
		params.Year = strconv.Itoa(time.Now().Year())
	}

	return params, nil
}

func UnmarshalParams(params *data.CopyrightInfo, a map[string][]string) error {
	return SchemaDecoder.Decode(params, a)
}

func ListLicenses(l string) string {
	switch l {
	case "list":
		fallthrough
	case "list.html":
		fallthrough
	case "list.htm":
		return theme.ListHtml(licenses)
	case "list.txt":
		return theme.ListTxt(licenses)
	case "list.json":
		return theme.ListJson(licenses)
	default:
		return "-"
	}
}

func getOwnersParams(params *data.CopyrightInfo) *data.CopyrightInfo {
	l := params.License
	y := params.Year
	params = ma124sParams()
	params.License = l
	params.Year = y
	return params
}

func fetchParams(urlup string) *data.CopyrightInfo {
	urlp, err := url.Parse("http://" + urlup) // TODO https://
	if err != nil {
		return nil
	}

	// TODO html meta tags

	cpi := fetchParamsFromLicenseApiTxt(urlp, false)
	if cpi != nil {
		return cpi
	}

	return fetchParamsFromLicenseApiTxt(urlp, true)
}

func fetchParamsFromLicenseApiTxt(urlp *url.URL, replace bool) *data.CopyrightInfo {
	path := assembleUrl(*urlp, "/license-api.json", replace)
	if !isRobotAllowed(assembleUrl(*urlp, "/robots.txt", true), path) {
		return nil
	}

	resp, err := http.Get(path)
	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	jsonData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil
	}

	cpi := &data.CopyrightInfo{}
	err = json.Unmarshal(jsonData, cpi)
	if err != nil {
		return nil
	}

	return cpi
}

func assembleUrl(u url.URL, appendedPath string, replacePath bool) string {
	if replacePath {
		u.Path = appendedPath
	} else {
		u.Path = path.Join(u.Path, appendedPath)
	}
	return u.String()
}

var robotTxtCacheObj *robotstxt.RobotsTxt
var robotTxtCacheUrl string

func isRobotAllowed(robotsTxtUrl, target string) bool {
	if !RobotsTxt {
		return true
	}

	if robotTxtCacheUrl == robotsTxtUrl {
		ok, err := robotTxtCacheObj.IsAllowed("Ma124-License-API/"+VERSION, target)
		if err != nil {
			return RobotsTxtErrAllow
		}

		return ok
	}

	resp, err := http.Get(robotsTxtUrl)
	if err != nil {
		return RobotsTxtErrAllow
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return true
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return RobotsTxtErrAllow
	}

	rtxt, err := robotstxt.Parse(string(data), robotsTxtUrl)
	if err != nil {
		return RobotsTxtErrAllow
	}

	robotTxtCacheObj = rtxt
	robotTxtCacheUrl = robotsTxtUrl

	ok, err := rtxt.IsAllowed("Ma124-License-API/"+VERSION, target)
	if err != nil {
		return RobotsTxtErrAllow
	}

	return ok
}
