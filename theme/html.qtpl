{% package theme %}

{% import (
    "gitlab.com/Ma_124/license-api/data"
) %}

{% stripspace %}
{% func Html(cpi *data.CopyrightInfo, li *data.LicenseInfo) %}
<html>
    <head>
        <meta charset="UTF-8" />
        {% comment %}Meta: Default Html{% endcomment %}
        <meta name="description" content="{%= copyStatementHtmlTxt(cpi, li) %}" />
        <meta name="generator" content="Ma124 License API" />
        <meta name="publisher" content="Ma_124" />
        <meta name="robots" content="none" />

        {% comment %}Link{% endcomment %}
        <link rel="stylesheet" href="https://mit-license.org/themes/dusk.css" title="Dusk" />
        <link rel="alternate stylesheet" href="https://mit-license.org/themes/default.css" title="Default" />
        <link rel="icon" href="https://www.gravatar.com/avatar/abf491ed3660046dcac5e853ad2e83ad?d=robohash" />

        {% comment %}Meta: Open Graph{% endcomment %}
        <meta name="og:title" content="{%= copyStatementHtmlTxt(cpi, li) %}" />
        <meta name="og:description" content="{%= copyStatementHtmlTxt(cpi, li) %}" />
        <meta name="og:type" content="profile" />
        <!-- TODO <meta name="og:url" content="" /> -->
        <meta name="og:image" content="https://www.gravatar.com/avatar/{%z gravatar(cpi.Mail) %}?d=robohash" />
        <meta name="og:profile:username" content="{%s cpi.Name %}" />
        <meta name="og:profile:gender" content="{%s cpi.Gender %}" />

        {% comment %}Meta: Twitter{% endcomment %}
        <meta name="twitter:card" content="summary" />
        {% if len(cpi.Twitter) > 0 %}
        {% if cpi.Twitter[0] == '@' %}
        <meta name="twitter:site" content="{%s cpi.Twitter %}" />
        {% else %}
        <meta name="twitter:site:id" content="{%s cpi.Twitter %}" />
        {% endif %}
        {% endif %}
        <meta name="twitter:title" content="{%= copyStatementHtmlTxt(cpi, li) %}" />
        <meta name="twitter:description" content="{%= copyStatementHtmlTxt(cpi, li) %}" />
        <meta name="twitter:image" content="https://www.gravatar.com/avatar/{%z gravatar(cpi.Mail) %}?d=robohash" />

        <title>{%= copyStatementHtmlTxt(cpi, li) %}</title>

        <style>
            .about-me {
                width: 100%;
            }

            .about-me td:nth-child(1), .license-deed td:nth-child(1) {
                width: 25%;
            }

            .about-me td:nth-child(2), .license-deed td:nth-child(2) {
                width: 75%;
            }

            .license-deed-value-indicator {
                width: 1.14em;
                height: 1.14em;
                -webkit-border-radius: 0.7em;
                -moz-border-radius: 0.7em;
                border-radius: 0.7em;
            }

            td {
                padding: 0.2em;
            }
        </style>
    </head>
    <body>
        <article>
            <img id="gravatar" src="https://www.gravatar.com/avatar/{%z gravatar(cpi.Mail) %}?d=robohash" />
            <h1>{%s li.Name %}</h1>

            <p class="copy-notice">
                {%= copyStatementHtml(cpi, li) %}
            </p>
            <p class="short-text" {% if li.FullText == "" %}id="full-text"{% endif %} style="white-space: pre-wrap;">{%s li.ShortText %}</p>
            {% if li.FullText != "" %}
            <details id="full-text">
                <summary>License Text</summary>
                <article><p class="full-text" style="white-space: pre-wrap;">{%s li.FullText %}</p></article>
            </details>
            {% endif %}
            {% if li.Deed != 0 %}
            {% comment %}Check License Deed{% endcomment %}
            <details>
                <summary>License Deed</summary>
                <div>
                    <strong><small>This is a human-readable summary of (and not a substitute for) the <a href="#full-text">license</a></small></strong>
                    <details>
                        <summary>Disclaimer</summary>
                        <div>
                            <p>This deed highlights only some of the key features and terms of the actual license. It is not a license and has no legal value. You should carefully review all of the terms and conditions of the actual license before using the licensed material.</p>
                            <p>Ma_124 is not a lawyer and does not provide legal services. Distributing, displaying, or linking to this deed or the license that it summarizes does not create a lawyer-client or any other relationship.</p>
                        </div>
                    </details>
                    <table class="license-deed">
                        <tbody>
                            {%= htmlDeedItem("Commercial use", "perm", li.Deed, data.CommercialUse) %}
                            {%= htmlDeedItem("Distribution", "perm", li.Deed, data.Distribution) %}
                            {%= htmlDeedItem("Modification", "perm", li.Deed, data.Modification) %}
                            {%= htmlDeedItem("Patent use", "perm", li.Deed, data.PatentUse1) %}
                            {%= htmlDeedItem("Private use", "perm", li.Deed, data.PrivateUse) %}
                            <tr></tr>
                            {%= htmlDeedItem("Disclose source", "cond", li.Deed, data.DiscloseSource) %}
                            {%= htmlDeedItem("License and copyright notice", "cond", li.Deed, data.LicenseAndCopyrightNotice) %}
                            {%= htmlDeedItem("Network use is distribution", "cond", li.Deed, data.NetworkUseIsDistribution) %}
                            {%= htmlDeedItem("Same license", "cond", li.Deed, data.SameLicense) %}
                            {%= htmlDeedItem("State changes", "cond", li.Deed, data.StateChanges) %}
                            <tr></tr>
                            {%= htmlDeedItem("Liability", "limit", ^li.Deed, data.Liability) %}
                            {%= htmlDeedItem("Trademark use", "limit", ^li.Deed, data.TrademarkUse) %}
                            {%= htmlDeedItem("Warranty", "limit", ^li.Deed, data.Warranty) %}
                        </tbody>
                    </table>
                <div>
            </details>
            {% endif %}
            <details open>
                <summary>About me</summary>
                <table class="about-me">
                    <tbody>
                        {% if len(cpi.Name) > 0 %}<tr><td>Name:</td><td>{%s cpi.Name %}</td></tr>{% endif %}
                        {% if len(cpi.RealName) > 0 %}<tr><td>Real Name:</td><td>{%s cpi.RealName %}</td></tr>{% endif %}
                        {% if len(cpi.Site) > 0 %}<tr><td>Site:</td><td><a href="https://{%j cpi.Site %}">{%s cpi.Site %}</a></td></tr>{% endif %}
                        {% if len(cpi.Mail) > 0 %}<tr><td>Mail:</td><td><a href="mailto:{%j cpi.Mail %}">{%s cpi.Mail %}</a></td></tr>{% endif %}
                        {% if len(cpi.Twitter) > 0 %}
                        {% if cpi.Twitter[0] == '@' %}
                        <tr><td>Twitter:</td><td><a href="https://twitter.com/{%j cpi.Twitter[1:] %}">{%s cpi.Twitter %}</a></td></tr>
                        {% else %}
                        <tr><td>Twitter:</td><td><a href="https://twitter.com/intent/user?user_id={%j cpi.Twitter %}">{%s cpi.Twitter %}</a></td></tr>
                        {% endif %}
                        {% endif %}
                    </tbody>
                </table>
            </details>
        </article>
        <footer>
        </footer>
    </body>
</html>
{% endfunc %}
{% endstripspace %}

{% stripspace %}
{% func htmlDeedItem(humanName, cat string, ld, it data.LicenseDeed)%}
<tr>
    <td>{%s humanName %}</td>
    {% if ld.Get(it) %}
    <td><div class="license-deed-value-indicator" style="background-color:
    {% switch cat %}
    {% case "perm" %}
    green
    {% case "cond" %}
    blue
    {% case "limit" %}
    red
    {% endswitch %}
    ;"></div></td>
    {% else %}
    <td></td>
    {% endif %}
</tr>
{% endfunc %}
{% endstripspace %}

{% stripspace %}
{% func copyStatementHtml(cpi *data.CopyrightInfo, li *data.LicenseInfo) %}
    {% if li.NeedsCopyright %}
    Copyright{% space %}&copy;{% space %}
    {%s cpi.Year %}{% space %}
    {% endif %}
    {% if cpi.Site != "" %}
        <a href="https://{%s cpi.Site %}">
    {% endif %}
    {%s cpi.Name %}
    {% if cpi.Site != "" %}
        </a>
    {% endif %}
    {% if cpi.Mail != "" %}
        {%space%}&lt;<a href="mailto:{%j cpi.Mail %}">{%s cpi.Mail %}</a>&gt;
    {% endif %}
{% endfunc %}
{% endstripspace %}

{% func copyStatementHtmlTxt(cpi *data.CopyrightInfo, li *data.LicenseInfo) %}{% if li.NeedsCopyright %}Copyright &copy; {%s cpi.Year %} {% endif %}{%s cpi.Name %}{% if cpi.Site != "" %}, {%s cpi.Site %}{% endif %}{% if cpi.Mail != "" %} <{%s cpi.Mail %}>{% endif %}{% endfunc %}
