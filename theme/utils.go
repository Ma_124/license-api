package theme

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	qt "github.com/valyala/quicktemplate"
	"io"
	"strings"
)

func gravatar(mail string) []byte {
	sum := md5.Sum(bytes.ToLower([]byte(bytes.TrimSpace([]byte(mail)))))
	return []byte(hex.EncodeToString(sum[:]))
}

func writeunesc(iow io.Writer, s string) {
	qw := qt.AcquireWriter(iow)
	streamunesc(qw, s)
	qt.ReleaseWriter(qw)
}

func unesc(s string) string {
	qb := qt.AcquireByteBuffer()
	writeunesc(qb, s)
	qs := string(qb.B)
	qt.ReleaseByteBuffer(qb)
	return qs
}

func streamunesc(qw *qt.Writer, s string) {
	qw.N().S(s)
}

func writewNl(iow io.Writer, s string) {
	qw := qt.AcquireWriter(iow)
	streamwNl(qw, s)
	qt.ReleaseWriter(qw)
}

func wNl(s string) string {
	qb := qt.AcquireByteBuffer()
	writewNl(qb, s)
	qs := string(qb.B)
	qt.ReleaseByteBuffer(qb)
	return qs
}

func streamwNl(qw *qt.Writer, s string) {
	qw.N().S(strings.Replace(s, "\n\n", "<br />", -1))
}
