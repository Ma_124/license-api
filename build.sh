#!/bin/sh
set -ev

go get github.com/valyala/quicktemplate/... github.com/aws/aws-lambda-go/events github.com/aws/aws-lambda-go/lambda github.com/samclarke/robotstxt gopkg.in/yaml.v2 gitlab.com/Ma_124/license-api/... gitlab.com/Ma_124/bitfielder

$GOPATH/bin/qtc -dir="theme"

mkdir -p funcs

go build -o funcs/l

