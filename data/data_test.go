package data

import (
	"github.com/davecgh/go-spew/spew"
	"gopkg.in/yaml.v2"
	"testing"
)

/*
	CommercialUse            bool `yaml:"com"`
	Distribution             bool `yaml:"dist"`
	Modification             bool `yaml:"mod"`
	PatentUse                rune `yaml:"patent"`
	PrivateUse               bool `yaml:"private"`
	DiscloseSource           bool `yaml:"src"`
	LicenseNotice            bool `yaml:"notice"`
	NetworkUseIsDistribution bool `yaml:"net"`
	SameLicense              bool `yaml:"same"`
	StateChanges             bool `yaml:"state-changes"`
	Liability                bool `yaml:"liability"`
	TrademarkUse             bool `yaml:"tm"`
	Warranty                 bool `yaml:"warranty"`
*/

func TestLicenseDeed_UnMarshalYAML(t *testing.T) {
	deed := NewLicenseDeed()
	deed.Put(CommercialUse)
	deed.Put(Distribution)
	deed.Put(Modification)
	deed.Put(PatentUse1)
	deed.Put(PatentUse2)
	deed.Put(PrivateUse)
	deed.Put(DiscloseSource)
	deed.Put(LicenseAndCopyrightNotice)
	deed.Put(NetworkUseIsDistribution)
	deed.Put(SameLicense)
	deed.Put(StateChanges)
	deed.Put(Liability)
	deed.Put(TrademarkUse)
	deed.Put(Warranty)

	out, err := yaml.Marshal(deed)
	if err != nil {
		panic(err)
	}

	deed2 := NewLicenseDeed()
	err = yaml.Unmarshal(out, &deed2)
	if err != nil {
		panic(err)
	}

	if deed != deed2 {
		spew.Dump(deed2)
		t.Fail()
	}
}
