package data

type CopyrightInfo struct {
	License  string `query:"-"          json:"-"`
	Name     string `query:"name"`
	Mail     string `query:"mail"`
	Site     string `query:"site"`
	RealName string `query:"realname"   json:"real-name"`
	Gender   string `query:"gender"`
	Twitter  string `query:"twitter"`
	Year     string `query:"year"       json:"-"`
}

type LicenseInfo struct {
	Id      string
	Aliases []string `yaml:"aliases,omitempty"`
	Name    string

	Deed           LicenseDeed `yaml:"deed,omitempty"`
	NeedsCopyright bool        `yaml:"needs-copyright"`

	ShortText string
	FullText  string
}

// Flags:
//  0 CommercialUse
//  1 Distribution
//  2 Modification
//  3 PatentUse1 (Y/N)
//  4 PatentUse2 (Unspecified)
//  5 PrivateUse
//  6 DiscloseSource
//  7 LicenseAndCopyrightNotice
//  8 NetworkUseIsDistribution
//  9 SameLicense
// 10 StateChanges
// 11 Liability
// 12 TrademarkUse
// 13 Warranty
type LicenseDeed uint16

/*const (
	CommercialUse uint32 = iota
	Distribution
	Modification
	PatentUseYN
	PatentUseUnspecified
	PrivateUse
	DiscloseSource
	LicenseNotice
	NetworkUseIsDistribution
	SameLicense
	StateChanges
	Liability
	TrademarkUse
	Warranty
)

func NewLicenseDeed() *LicenseDeed {
	return &LicenseDeed{
		bitfield.New(14),
	}
}

type licenseDeedStruct struct {
	CommercialUse            bool `yaml:"com"`
	Distribution             bool `yaml:"dist"`
	Modification             bool `yaml:"mod"`
	PatentUse                rune `yaml:"patent"`
	PrivateUse               bool `yaml:"private"`
	DiscloseSource           bool `yaml:"src"`
	LicenseNotice            bool `yaml:"notice"`
	NetworkUseIsDistribution bool `yaml:"net"`
	SameLicense              bool `yaml:"same"`
	StateChanges             bool `yaml:"state-changes"`
	Liability                bool `yaml:"liability"`
	TrademarkUse             bool `yaml:"tm"`
	Warranty                 bool `yaml:"warranty"`
}

func (l *LicenseDeed) UnmarshalYAML(unmarshal func(interface{}) error) error {
	c := &licenseDeedStruct{}

	err := unmarshal(c)
	if err != nil {
		panic(err)
	}

	if c.CommercialUse {
		l.Set(CommercialUse)
	}

	if c.Distribution {
		l.Set(Distribution)
	}

	if c.Modification {
		l.Set(Modification)
	}

	switch c.PatentUse {
	case 0:
		fallthrough
	case 'u':
		l.Set(PatentUseUnspecified)
	case 'y':
		l.Set(PatentUseYN)
	case 'n':
	default:
		return errors.New("unknown value for PatentUse (can only be u, y or n)")
	}

	if c.PrivateUse {
		l.Set(PrivateUse)
	}

	if c.DiscloseSource {
		l.Set(DiscloseSource)
	}

	if c.LicenseNotice {
		l.Set(LicenseNotice)
	}

	if c.NetworkUseIsDistribution {
		l.Set(NetworkUseIsDistribution)
	}

	if c.SameLicense {
		l.Set(SameLicense)
	}

	if c.StateChanges {
		l.Set(StateChanges)
	}

	if c.Liability {
		l.Set(Liability)
	}

	if c.TrademarkUse {
		l.Set(TrademarkUse)
	}

	if c.Warranty {
		l.Set(Warranty)
	}

	return nil
}

func (l *LicenseDeed) MarshalYAML(marshal func(interface{}) error) error {
	aux := &licenseDeedStruct{
		l.Test(CommercialUse),
		l.Test(Distribution),
		l.Test(Modification),
		'n',
		l.Test(PrivateUse),
		l.Test(DiscloseSource),
		l.Test(LicenseNotice),
		l.Test(NetworkUseIsDistribution),
		l.Test(SameLicense),
		l.Test(StateChanges),
		l.Test(Liability),
		l.Test(TrademarkUse),
		l.Test(Warranty),
	}

	if l.Test(PatentUseUnspecified) {
		aux.PatentUse = 'u'
	}

	if l.Test(PatentUseYN) {
		aux.PatentUse = 'y'
	}

	marshal(aux)

	return nil
}*/
