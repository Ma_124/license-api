package main

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/Ma_124/license-api/data"
	"io/ioutil"
	"net/http/httptest"
	"strings"
	"testing"
)

var serv = httpHandler{}

func TestHttpHandler_ServeHTTPErrs(t *testing.T) {
	testHttpRequest(t, "unknown loc (/)", "/", 400, `calling from unknown URL (root)`)
	testHttpRequest(t, "unknown loc (/a)", "/a", 400, `calling from unknown URL`)
	testHttpRequest(t, "no license", "/l/", 400, `no license name specified`)
	testHttpRequest(t, "unknown license", "/l/example", 400, `unknown license: example`)
	testHttpRequest(t, "unknown type", "/l/mit.cpp", 400, `unknown license: mit`) // TODO
}

func testHttpRequest(t *testing.T, name, target string, expectedStatus int, expectedBody string) {
	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", target, nil)

	serv.ServeHTTP(rec, req)

	resp := rec.Result()

	assert.Equalf(t, expectedStatus, resp.StatusCode, "%s: wrong status code", name)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		assert.Errorf(t, err, "%s: error while reading the body", name)
	}
	assert.Equalf(t, expectedBody, string(body), "%s: wrong body", name)
}

func TestRender(t *testing.T) {
	err := loadLicense("mit")
	assert.Equalf(t, nil, err, "can't load mit")

	cpi := &data.CopyrightInfo{
		"mit",
		"Name",
		"name@example.com",
		"example.com",
		"Real Name",
		"male",
		"@twitter",
		"2019",
	}

	s, err := Render(cpi)
	assert.Equalf(t, nil, err, "render def: err")
	assert.Equalf(t, true, strings.HasPrefix(s, "<html><head>"), "render def: result")

	cpi.License = "mit.html"
	s, err = Render(cpi)
	assert.Equalf(t, nil, err, "render .html: err")
	assert.Equalf(t, true, strings.HasPrefix(s, "<html><head>"), "render .html: result")

	cpi.License = "mit.htm"
	s, err = Render(cpi)
	assert.Equalf(t, nil, err, "render .htm: err")
	assert.Equalf(t, true, strings.HasPrefix(s, "<html><head>"), "render .htm: result")

	cpi.License = "mit.txt"
	s, err = Render(cpi)
	assert.Equalf(t, nil, err, "render .txt: err")
	assert.Equalf(t, true, strings.HasPrefix(s, "Copyright (c)"), "render .txt: result")
}
