package main

import (
	"errors"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Ma_124/license-api/data"
	"gitlab.com/Ma_124/license-api/theme"
	"gopkg.in/yaml.v2"
)

const VERSION = "v1.0"

func main() {
	fis, err := ioutil.ReadDir("license")
	if err != nil {
		panic(err)
	}

	for _, fi := range fis {
		if strings.HasSuffix(fi.Name(), ".yaml") {
			err := loadLicense(fi.Name()[:len(fi.Name())-5])
			if err != nil {
				panic(err)
			}
		}
	}

	ignoreRobotsTxt := flag.Bool("ignore-robotstxt", false, "")
	notRobotsTxtErrAllow := flag.Bool("fail-on-invalid-robotstxt", false, "")
	localServer := flag.Bool("server", false, "")
	host := flag.String("host", ":8080", "")
	flag.Parse()

	if *ignoreRobotsTxt {
		RobotsTxt = false
	}

	if *notRobotsTxtErrAllow {
		RobotsTxtErrAllow = false
	}

	if *localServer {
		s := http.Server{
			Addr:    *host,
			Handler: httpHandler{},
		}
		err = s.ListenAndServe()
		if err != nil {
			panic(err)
		}
	} else {
		_ = os.Chdir("..")
		lambda.Start(LambdaHandler)
	}
}

func loadLicense(name string) error {
	yml, err := ioutil.ReadFile("license/" + name + ".yaml")
	if err != nil {
		return err
	}

	li := &data.LicenseInfo{}
	err = yaml.Unmarshal(yml, li)
	if err != nil {
		return err
	}

	txt, err := ioutil.ReadFile("license/" + name + "_short.txt")
	if err != nil {
		return err
	}

	li.ShortText = string(txt)

	txt, err = ioutil.ReadFile("license/" + name + "_full.txt")
	if err == nil {
		li.FullText = string(txt)
	} else if !os.IsNotExist(err) {
		return err
	}

	li.Deed = 0

	licenses[li.Id] = li
	for _, key := range li.Aliases {
		licenses[key] = li
	}

	return nil
}

var licenses = make(map[string]*data.LicenseInfo)

func Render(cpi *data.CopyrightInfo) (string, error) {
	lnameps := strings.SplitN(cpi.License, ".", 2)

	l := licenses[lnameps[0]]

	if l == nil {
		return "", errors.New("unknown license: " + lnameps[0])
	}

	var s string

	if len(lnameps) == 1 {
		s = theme.Html(cpi, l)
	} else {
		switch lnameps[1] {
		case "txt":
			s = theme.Txt(cpi, l)
		case "html":
			fallthrough
		case "htm":
			s = theme.Html(cpi, l)
		default:
			// TODO handle gpl-3.0
			return "", errors.New("unknown type")
		}
	}

	return s, nil
}

func Handler(method, path, remote string, query map[string][]string) (s string, status int) {
	lvl := log.InfoLevel
	status = 200
	msg := ""
	var err error

	le := log.WithFields(map[string]interface{}{
		"method": method,
		"path":   path,
		"ip":     remote,
	})
	defer le.WithFields(map[string]interface{}{"status": status, "error": err}).Log(lvl, msg)

	if strings.HasPrefix(path, "/favicon") || strings.HasPrefix(path, "favicon") {
		status = http.StatusNotFound
		msg = "requested favicon"
		return
	}

	cpi, err := GetParams(path)
	if err != nil {
		status = http.StatusBadRequest
		s = err.Error()
		msg = "error: GetParams"
		return
	}
	if cpi == nil {
		status = http.StatusInternalServerError
		s = "Internal Error: nil copyright info"
		msg = "nil cpi"
		lvl = log.WarnLevel
		return
	}

	if s = ListLicenses(cpi.License); s != "-" {
		return
	}

	err = UnmarshalParams(cpi, query)
	if err != nil {
		status = http.StatusBadRequest
		s = err.Error()
		msg = "error: UnmarshalParams"
		return
	}

	s, err = Render(cpi)
	if err != nil {
		status = http.StatusBadRequest
		s = err.Error()
		msg = "error: Render"
		return
	}

	msg = "served"
	return
}

type httpHandler struct{}

func (httpHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	s, status := Handler(req.Method, req.URL.Path, req.RemoteAddr, req.URL.Query())
	resp.WriteHeader(status)
	_, err := resp.Write([]byte(s))
	if err != nil {
		log.Error(err)
	}
}

func LambdaHandler(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	s, status := Handler(req.HTTPMethod, req.Path, req.RequestContext.Identity.SourceIP, req.MultiValueQueryStringParameters)
	return &events.APIGatewayProxyResponse{
		StatusCode: status,
		Body: s,
	}, nil
}
